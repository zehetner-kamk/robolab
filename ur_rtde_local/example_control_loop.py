#!/usr/bin/env python
# Copyright (c) 2016, Universal Robots A/S,
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the Universal Robots A/S nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
# Adapted by Clemens Zehetner 🄯 2021


import sys
import socket
import time
import rtde.rtde as rtde
import rtde.rtde_config as rtde_config


# logging.basicConfig(level=logging.INFO)

ROBOT_HOST = '10.50.90.5'
ROBOT_PORT = 30004
config_filename = 'control_loop_configuration.xml'


conf = rtde_config.ConfigFile(config_filename)
state_names, state_types = conf.get_recipe('state')

con = rtde.RTDE(ROBOT_HOST, ROBOT_PORT)
con.connect()

# get controller version
con.get_controller_version()

# setup recipes
con.send_output_setup(state_names, state_types)  # used for getting data from robot
# setp = con.send_input_setup(setp_names, setp_types) #used for sending data to robot, not necessary here
#watchdog = con.send_input_setup(watchdog_names, watchdog_types)


def setp_to_list(setp):
    list = []
    for i in range(0, 6):
        list.append(setp.__dict__["input_double_register_%i" % i])
    return list


def list_to_setp(setp, list):
    for i in range(0, 6):
        setp.__dict__["input_double_register_%i" % i] = list[i]
    return setp


def vector2str(list):
    s = ""
    for i in list:
        s += str(i)+","

    return s[0:len(s)-1]  # remove last ,


# start data synchronization
if not con.send_start():
    sys.exit()

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
while True:
    # receive the current state
    state = con.receive()

    data = "UR;" + ROBOT_HOST + ";" + str(int(time.time()*1000)) + ";"  # universal robot; IP address also functions as robot id; time in ms
    data += "actual_TCP_pose:VECTOR6D:" + vector2str(state.actual_TCP_pose) + ";"  # params can be defined in control_loop_configuration.xml
    data += "timestamp:DOUBLE:" + str(state.timestamp) + ";"
    data += "actual_digital_input_bits:UINT64:" + str(state.actual_digital_input_bits) + ";"
    data += "joint_mode:VECTOR6INT32:" + vector2str(state.joint_mode) + ";"
    data += "safety_status:INT32:" + str(state.safety_status) + ";"
    data += "actual_tool_accelerometer:VECTOR3D:" + vector2str(state.actual_tool_accelerometer) + ";"
    data += "output_bit_register_69:BOOL:" + str(state.output_bit_register_69) + ";"
    data += "tool_output_mode:UINT8:" + str(state.tool_output_mode) + ";"
    data += "input_bit_registers0_to_31:UINT32:" + str(state.input_bit_registers0_to_31) + ";"
    # add more params here ...

    # generate primitive checksum. this makes server less complex since it can now be fairly sure that the message was sent from this script and doesnt need to validate every input
    #sum = 0
    # for d in data:
    #    sum += ord(d)
    #sum = int(sum/int(ROBOT_HOST.split(".")[0]))
    #data += "|" + str(sum)

    sock.sendto(data.encode('utf-8'), ("128.214.252.194", 30040))  # set server IP here

    if state is None:
        break


con.send_pause()
con.disconnect()
