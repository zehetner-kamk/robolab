cd /home/ubuntu
sudo apt update
sudo apt install openjdk-11-jre-headless --yes

#change this to latest version
wget https://dev.mysql.com/get/mysql-apt-config_0.8.17-1_all.deb
sudo dpkg -i mysql-apt-config_0.8.17-1_all.deb
sudo apt update
sudo apt install mysql-server -yes

echo "Please enter the content of create-insert.sql into the database terminal."
sudo mysql -u root -p

#jdbc config files
mkdir config && cd config
touch dburl | echo "mysql://robodb:3306/" > dburl
touch dbname | echo "robodb" > dbname
touch dbuser | echo "robo" > dbuser
touch dbpass | echo "b2xL45j5M6ZDaGftSP9c" > dbpass


#grafana
sudo apt-get install -y apt-transport-https
sudo apt-get install -y software-properties-common wget
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
sudo apt update
sudo apt install grafana
sudo service grafana-server start

