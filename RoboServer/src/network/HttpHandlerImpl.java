package network;

import com.sun.net.httpserver.HttpExchange;
import logic.MessageHandler;

import java.io.IOException;
import java.util.HashMap;
/*
public class HttpHandlerImpl implements com.sun.net.httpserver.HttpHandler {
    private MessageHandler handler;

    public HttpHandlerImpl(MessageHandler handler) {
        this.handler = handler;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException { //gets called when http request is made to server

        if (httpExchange.getRequestMethod().equals("POST")) { //only post requests are supported atm
            //TODO: INSERT GET REQUEST FOR DOCUMENTATION
            this.handlePost(httpExchange);
        } else {
            httpExchange.sendResponseHeaders(400, 0);
        }
        httpExchange.close();
    }


    private void handlePost(HttpExchange httpExchange) {
        HashMap<String, String> params = getParams(httpExchange.getRequestURI().toString());

        try {
            if(params.containsKey("type") && params.get("type").equals("storeData")){
                String content = httpExchange.getRequestBody().toString();


                handler.enqueueMessage(new Message());
            }else{
                httpExchange.sendResponseHeaders(400, 0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private HashMap<String, String> getParams(String url) { //parses url parameters
        if (!url.contains("?")) return new HashMap<>(); //no parameters in url

        String[] rawParams = url.split("\\?")[1].split("&");
        HashMap<String, String> params = new HashMap<>();

        for (String p : rawParams) {
            String[] kv = p.split("="); //key-value
            params.putIfAbsent(kv[0], kv[1]);
        }

        return params;
    }
}
*/