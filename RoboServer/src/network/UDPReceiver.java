package network;

import logic.MessageHandler;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UDPReceiver {
    private static boolean stop;
    private static final int RECEIVE_PORT = 30040;
    private static MessageHandler handler;

    public static void start() { //endless while loop that receives UDP packets and notifies messageHandlers
        DatagramSocket socket = null;
        stop = false;
        while (!stop) {
            try {
                while (socket == null || socket.isClosed()) {
                    socket = new DatagramSocket(RECEIVE_PORT);
                }
                DatagramPacket packet = new DatagramPacket(new byte[10000], 10000);
                socket.receive(packet);

                String content = (new String(packet.getData(), "UTF-8")).replace("\0", "");
                //System.out.println("Received " + content + packet.getAddress() + packet.getPort());

                Message m = new Message(content, packet.getAddress().toString().substring(1), packet.getPort(), null, RECEIVE_PORT);
                handler.enqueueMessage(m);
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored) {
                }
            }
        }
    }

    public static void stop() {
        stop = true;
    }

    public static void setMessageHandler(MessageHandler h) {
        handler = h;
    }
}