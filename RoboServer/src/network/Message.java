package network;

public class Message {
    private String content;
    private String sourceIP;
    private Integer sourcePort;
    private String destinationIP;
    private Integer destinationPort;


    public Message(String content, String sourceIP, Integer sourcePort, String destinationIP, Integer destinationPort) {
        this.content = content;
        this.sourceIP = sourceIP;
        this.sourcePort = sourcePort;
        this.destinationIP = destinationIP;
        this.destinationPort = destinationPort;
    }

    public Message(Message m) {
        this.content = m.getContent();
        this.sourceIP = m.getSourceIP();
        this.sourcePort = Integer.valueOf(m.getSourcePort());
        this.destinationIP = m.getDestinationIP();
        this.destinationPort = Integer.valueOf(m.getDestinationPort());
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSourceIP() {
        return sourceIP;
    }

    public Integer getSourcePort() {
        return sourcePort;
    }

    public String getDestinationIP() {
        return destinationIP;
    }

    public Integer getDestinationPort() {
        return destinationPort;
    }

}
