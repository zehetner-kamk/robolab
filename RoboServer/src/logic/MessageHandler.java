package logic;

import network.Message;

public interface MessageHandler {
    void enqueueMessage(Message m);
}
