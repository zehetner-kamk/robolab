package logic;

import java.util.LinkedList;
import java.util.List;

public interface RoboHandler {

    public void close();

    public LinkedList<List<String>> getDataQ();

    public long getLastDataTime();
}
