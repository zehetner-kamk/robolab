package logic.impl;

import database.URDao;
import database.domainObjects.URDoubleDO;
import database.domainObjects.URLongDO;
import database.domainObjects.URVectorDO;
import logic.MessageHandlerAbstract;
import logic.RoboHandler;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class URRoboHandler extends MessageHandlerAbstract implements RoboHandler {
    private String roboID;
    private String ip; //ip and port of sending computer, not of robot
    private int port;
    private LinkedList<List<String>> dataQ = new LinkedList<>();
    private URDao urDao = new URDao();
    private long lastDataTime;
    private boolean active = true; //active connection

    public URRoboHandler(String roboID, String ip, int port) {
        this.roboID = roboID;
        this.ip = ip;
        this.port = port;
        new Thread(this::pollMessage).start();
    }


    private void pollMessage() { //polls messages that get put by GenericDataHandler and stores them into Database
        while (active) {
            List<String> params = dataQ.pollFirst(); //FORMAT manufacturer;roboID;time;params (fieldname:datatype:data);|checksum
            if (params != null) {
                long time = parseLong(params.get(2), roboID, ip, port);

                if (time != -2) {
                    lastDataTime = time;

                    for (String p : params.subList(3, params.size())) {
                        String[] fields = p.split(":"); //one param consists of fieldname:datatype:data

                        if (fields.length == 3) {
                            switch (fields[1]) {
                                case "BOOL":
                                    long b = 0;
                                    if (fields[2].equals("True")) b = 1;
                                    urDao.insertURData(new URLongDO(roboID, time, fields[0], fields[1], b));
                                    break;
                                case "INT32":
                                case "UINT8":
                                case "UINT32":
                                case "UINT64": //WARNING: UINT64 may be out of bounds with long
                                    long l = parseLong(fields[2], roboID, ip, port);
                                    urDao.insertURData(new URLongDO(roboID, time, fields[0], fields[1], l));
                                    break;
                                case "DOUBLE":
                                    double d = parseDouble(fields[2], roboID, ip, port);
                                    urDao.insertURData(new URDoubleDO(roboID, time, fields[0], fields[1], d));
                                    break;
                                case "VECTOR3D":
                                case "VECTOR6D":
                                case "VECTOR6INT32":
                                    ArrayList<Double> v = new ArrayList<>(6);
                                    for (int i = 0; i < fields[2].split(",").length; ++i) {
                                        v.add(parseDouble(fields[2].split(",")[i], roboID, ip, port));
                                    }
                                    if ((v.size() == 3 || v.size() == 6)) urDao.insertURData(new URVectorDO(roboID, lastDataTime, fields[0], fields[1], v));
                                    break;
                            }
                        }
                    }
                }
            }
            try {
                Thread.sleep(5); //max 200 r/s
            } catch (InterruptedException e) {
            }
        }
    }


    public void close() {
        this.active = false;
    }

    public LinkedList<List<String>> getDataQ() {
        return dataQ;
    }

    public long getLastDataTime() {
        return this.lastDataTime;
    }
}