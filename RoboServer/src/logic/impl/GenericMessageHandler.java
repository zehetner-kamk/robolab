package logic.impl;

import logic.MessageHandler;
import logic.MessageHandlerAbstract;
import logic.RoboHandler;
import network.Message;
import network.UDPReceiver;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class GenericMessageHandler extends MessageHandlerAbstract implements MessageHandler {
    private static final int TIMEOUT_MINS = 10; //robots get timeouted after being inactive
    private static LinkedList<Message> genericMsgQ = new LinkedList<>();
    private static Map<String, RoboHandler> roboHandlers = new HashMap<>();


    static{
        new Thread(GenericMessageHandler::handleMessage).start();
        new Thread(GenericMessageHandler::timeoutHandlers).start();
    }

    public GenericMessageHandler() {
        UDPReceiver.setMessageHandler(this);
    }


    public void enqueueMessage(Message m) { //all received messages land here and get queued
        //this is to ensure maximum performance, since UDPReceiver thread doesn't do anything with the messages besides storing them
        if (m == null) return;
        synchronized (genericMsgQ) {
            genericMsgQ.add(m);
        }
    }

    private static void handleMessage() { //creates and destroys message handlers for every connected robot
        while (true) {
            Message m;
            synchronized (genericMsgQ) {
                m = genericMsgQ.pollFirst();
            }

            if (m != null) {
                ArrayList<String> params = new ArrayList<>(Arrays.asList(m.getContent().split(";")));

                if (params.size() >= 4) { //FORMAT manufacturer;roboID;time;params (field:field:field);
                    //at least manufacturer, roboID, time and 1 param have to exist

                    /*int checksumMsg = parseInteger(params.get(params.size() - 1).substring(1), "undefined", m.getSourceIP(), m.getSourcePort()); //checksum in message
                    String[] ipArray = params.get(1).split("\\.");

                    int divider = -2;
                    if (ipArray.length == 4) divider = parseInteger(ipArray[0], "undefined", m.getSourceIP(), m.getSourcePort()); //divider is first num of IP

                    int checksumCalc = 0; //checksum calculated with message content
                    for (int i = 0; i < m.getContent().split("\\|")[0].length(); ++i) {
                        checksumCalc += m.getContent().charAt(i);
                    }
                    checksumCalc /= divider;

                    if (checksumMsg == checksumCalc) { //integers have been parsed correctly and checksum matches -> data is probably sent in correct format */
                    //reduces the need for server side data validation

                    String roboID = params.get(1); //IP address is roboID
                    if (!roboHandlers.containsKey(roboID)) {
                        switch (params.get(0)) { //different handlers for different manufacturers
                            case "UR":
                                roboHandlers.put(roboID, new URRoboHandler(roboID, m.getSourceIP(), m.getSourcePort()));
                                break;
                            case "ABB:": //ABBRoboHandler is not implemented yet
                                roboHandlers.put(roboID, new ABBRoboHandler(roboID, m.getSourceIP(), m.getSourcePort()));
                                break;
                            //add robohandlers for other manufacturers
                        }
                    }
                    roboHandlers.get(roboID).getDataQ().add(params);

                    /*} else {
                        UDPSender.send(new Message("Invalid request.", null, null, m.getSourceIP(), m.getSourcePort()));
                    }*/
                }
            }
        }
    }

    private static void timeoutHandlers() { //robots get timeouted after being inactive
        while (true) {
            synchronized (roboHandlers) {
                List<String> keys = new ArrayList<>(roboHandlers.keySet());

                for (int i = 0; i < keys.size(); ++i) {
                    if (System.currentTimeMillis() - roboHandlers.get(keys.get(i)).getLastDataTime() > TimeUnit.MINUTES.toMillis(TIMEOUT_MINS)) {
                        roboHandlers.get(keys.get(i)).close(); //closes thread and removes it after robot was inactive
                        roboHandlers.remove(keys.get(i));
                    }
                }
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
        }
    }


}
