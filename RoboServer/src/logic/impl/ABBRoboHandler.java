package logic.impl;

import logic.MessageHandlerAbstract;
import logic.RoboHandler;

import java.util.LinkedList;
import java.util.List;

public class ABBRoboHandler extends MessageHandlerAbstract implements RoboHandler {
    private String roboID;
    private String ip; //ip and port of sending computer, not of robot
    private int port;
    private LinkedList<List<String>> dataQ = new LinkedList<>();
    private long lastDataTime;
    private boolean active = true; //active connection

    public ABBRoboHandler(String roboID, String ip, int port) {
        this.roboID = roboID;
        this.ip = ip;
        this.port = port;
        new Thread(this::pollMessage).start();
    }


    private void pollMessage() { //polls messages that get put by GenericDataHandler and stores them into Database
        while (active) {
            List<String> params = dataQ.pollFirst();
            if (params != null) {


            }
            try {
                Thread.sleep(1); //max 1000 r/s
            } catch (InterruptedException e) {
            }
        }
    }

    public void close() {
        this.active = false;
    }

    public LinkedList<List<String>> getDataQ() {
        return dataQ;
    }

    public long getLastDataTime() {
        return this.lastDataTime;
    }
}