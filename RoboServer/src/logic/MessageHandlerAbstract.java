package logic;


import network.Message;
import network.UDPSender;

public abstract class MessageHandlerAbstract {

    protected int parseInteger(String s, String roboID, String ip, int port) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            UDPSender.send(new Message(roboID + ",Integer " + s + " contains invalid characters.", null, null, ip, port));
            return -2;
        }
    }

    protected double parseDouble(String s, String roboID, String ip, int port) {
        try {
            return Double.parseDouble(s);
        } catch (NumberFormatException e) {
            UDPSender.send(new Message(roboID + ",Double " + s + " contains invalid characters.", null, null, ip, port));
            return -2.0;
        }
    }

    protected long parseLong(String s, String roboID, String ip, int port) {
        try {
            return Long.parseLong(s);
        } catch (NumberFormatException e) {
            UDPSender.send(new Message(roboID + ",Long " + s + " contains invalid characters.", null, null, ip, port));
            return -2;
        }
    }


}
