import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import logic.MessageHandler;
import logic.impl.GenericMessageHandler;
import network.UDPReceiver;
import sun.net.httpserver.HttpServerImpl;

import java.net.InetSocketAddress;

public class RoboServer {
    private static int SERVER_PORT = 30040;

    public static void main(String[] args) {
        System.out.println("Starting Backend ...");

        MessageHandler genericMessageHandler = new GenericMessageHandler();

        /*
        HttpServer httpServer = null;
        while (httpServer == null) {
            try {
                httpServer = HttpServerImpl.create(new InetSocketAddress(SERVER_PORT), 0);
                HttpContext context = httpServer.createContext("/");
                context.setHandler(new HttpHandlerImpl(genericMessageHandler));

                httpServer.start();
            } catch (Exception e) {
                e.printStackTrace();

                if (httpServer != null) { //stops and resets httpServer, so it starts itself again after 10 sec
                    httpServer.stop(0);
                    httpServer = null;
                }
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
        }*/

        UDPReceiver.start();

        //TODO: copy httpserver from rubberduck and make python code send post requests
    }
}
