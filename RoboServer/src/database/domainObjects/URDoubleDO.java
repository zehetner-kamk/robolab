package database.domainObjects;

public class URDoubleDO {
    String roboID;
    long time;
    String fieldName;
    String datatype;
    double data;


    public URDoubleDO(String roboID, long time, String fieldName, String datatype, double data) {
        this.roboID = roboID;
        this.time = time;
        this.fieldName = fieldName;
        this.datatype=datatype;
        this.data = data;
    }


    public String getRoboID() {
        return roboID;
    }

    public void setRoboID(String roboID) {
        this.roboID = roboID;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public double getData() {
        return data;
    }

    public void setData(double data) {
        this.data = data;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}

