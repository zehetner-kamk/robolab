package database.domainObjects;

import java.util.List;

public class URLongDO {
    String roboID;
    long time;
    String fieldName;
    String datatype;
    long data;


    public URLongDO(String roboID, long time, String fieldName, String datatype, long data) {
        this.roboID = roboID;
        this.time = time;
        this.fieldName = fieldName;
        this.datatype=datatype;
        this.data = data;
    }


    public String getRoboID() {
        return roboID;
    }

    public void setRoboID(String roboID) {
        this.roboID = roboID;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public long getData() {
        return data;
    }

    public void setData(long data) {
        this.data = data;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}

