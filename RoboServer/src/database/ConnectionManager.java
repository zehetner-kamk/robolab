package database;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class ConnectionManager {
    private static final String CONFIG_LOCATION = "/home/ubuntu/config/"; //location of the config files
    private static final String DB_URL = readConfig(CONFIG_LOCATION + "dburl");
    private static final String DB_NAME = readConfig(CONFIG_LOCATION + "dbname");
    private static final String DB_USER = readConfig(CONFIG_LOCATION + "dbuser");
    private static final String DB_PASS = readConfig(CONFIG_LOCATION + "dbpass");
    private static Connection connection;

    static {
        try {
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static Connection getConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                connection = DriverManager.getConnection("jdbc:" + DB_URL + DB_NAME, DB_USER, DB_PASS);
            }
            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null; //could not create connection
    }

    private static String readConfig(String filename) {
        try {
            Scanner s = new Scanner(new File(filename));
            return s.nextLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }
}
