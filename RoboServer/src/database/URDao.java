package database;


import database.domainObjects.URDoubleDO;
import database.domainObjects.URLongDO;
import database.domainObjects.URVectorDO;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

public class URDao {
    private static final String LONG_TABLE_NAME = "ur_long";
    private static final String DOUBLE_TABLE_NAME = "ur_double";
    private static final String VECTOR_TABLE_NAME = "ur_vector";

    /*
    private static final int MAX_BUFFER_SIZE = 100;
    private static List<URLongDO> longDOBuffer = new ArrayList<>((int) (MAX_BUFFER_SIZE * 1.1));
    private static List<URDoubleDO> doubleDOBuffer = new ArrayList<>((int) (MAX_BUFFER_SIZE * 1.1));
    private static List<URVectorDO> vectorDOBuffer = new ArrayList<>((int) (MAX_BUFFER_SIZE * 1.1));

    static { //insert statements get processed in separate thread.
        new Thread(URDao::insertURData).start();
    }

    public void enqueueURData(URLongDO record) {
        synchronized (longDOBuffer) {
            longDOBuffer.add(record);
        }
    }

    public void enqueueURData(URDoubleDO record) {
        synchronized (doubleDOBuffer) {
            doubleDOBuffer.add(record);
        }
    }

    public void enqueueURData(URVectorDO record) {
        synchronized (vectorDOBuffer) {
            vectorDOBuffer.add(record);
        }
    }

    private static void insertURData() {
        String longStatement = "INSERT INTO " + LONG_TABLE_NAME + " (roboid, time, fieldname, datatype, a) VALUES (?, ?, ?, ?, ?)";
        String doubleStatement = "INSERT INTO " + DOUBLE_TABLE_NAME + " (roboid, time, fieldname, datatype, a) VALUES (?, ?, ?, ?, ?)";
        String vectorStatement = "INSERT INTO " + VECTOR_TABLE_NAME + " (roboid, time, fieldname, datatype, a, b, c, d, e, f) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";


        while (true) {
            if (ConnectionManager.getConnection() != null) {
                synchronized (longDOBuffer) {
                    if (longDOBuffer.size() >= MAX_BUFFER_SIZE) {
                        try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(longStatement)) {
                            for (URLongDO l : longDOBuffer) {
                                statement.setString(1, l.getRoboID()); //counting here is 1-based for some reason
                                statement.setTimestamp(2, new Timestamp(l.getTime()));
                                statement.setString(3, l.getFieldName());
                                statement.setString(4, l.getDatatype());
                                statement.setLong(5, l.getData());
                                statement.addBatch();
                            }
                            longDOBuffer.clear();

                            statement.executeBatch();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
                synchronized (doubleDOBuffer) {
                    if (doubleDOBuffer.size() >= MAX_BUFFER_SIZE) {
                        try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(doubleStatement)) {
                            for (URDoubleDO d : doubleDOBuffer) {
                                statement.setString(1, d.getRoboID()); //counting here is 1-based for some reason
                                statement.setTimestamp(2, new Timestamp(d.getTime()));
                                statement.setString(3, d.getFieldName());
                                statement.setString(4, d.getDatatype());
                                statement.setDouble(5, d.getData());
                                statement.addBatch();
                            }
                            doubleDOBuffer.clear();

                            statement.executeBatch();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            synchronized (vectorDOBuffer) {
                if (vectorDOBuffer.size() >= MAX_BUFFER_SIZE) {
                    try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(vectorStatement)) {
                        for (URVectorDO v : vectorDOBuffer) {
                            boolean threeD = v.getData().size() == 3;
                            statement.setString(1, v.getRoboID()); //counting here is 1-based for some reason
                            statement.setTimestamp(2, new Timestamp(v.getTime()));
                            statement.setString(3, v.getFieldName());
                            statement.setString(4, v.getDatatype());
                            for (int i = 0; i < v.getData().size(); ++i) {
                                statement.setDouble(i + 5, v.getData().get(i));
                            }
                            if (threeD) {
                                statement.setNull(8, Types.NULL);
                                statement.setNull(9, Types.NULL);
                                statement.setNull(10, Types.NULL);
                            }
                            statement.addBatch();
                        }
                        vectorDOBuffer.clear();

                        statement.executeBatch();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
            }
        }
    }
*/




    public boolean insertURData(URLongDO record) {
        String insertStatement = "INSERT INTO " + LONG_TABLE_NAME + " (roboid, time, fieldname, datatype, a) VALUES (?, ?, ?, ?, ?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {
                statement.setString(1, record.getRoboID()); //counting here is 1-based for some reason
                statement.setTimestamp(2, new Timestamp(record.getTime()));
                statement.setString(3, record.getFieldName());
                statement.setString(4, record.getDatatype());
                statement.setLong(5, record.getData());

                statement.execute();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean insertURData(URDoubleDO record) {
        String insertStatement = "INSERT INTO " + DOUBLE_TABLE_NAME + " (roboid, time, fieldname, datatype, a) VALUES (?, ?, ?, ?, ?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {
                statement.setString(1, record.getRoboID()); //counting here is 1-based for some reason
                statement.setTimestamp(2, new Timestamp(record.getTime()));
                statement.setString(3, record.getFieldName());
                statement.setString(4, record.getDatatype());
                statement.setDouble(5, record.getData());

                statement.execute();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean insertURData(URVectorDO record) {
        boolean threeD = record.getData().size() == 3;

        String insertStatement = "INSERT INTO " + VECTOR_TABLE_NAME + " (roboid, time, fieldname, datatype, a, b, c, d, e, f) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";


        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {
                statement.setString(1, record.getRoboID()); //counting here is 1-based for some reason
                statement.setTimestamp(2, new Timestamp(record.getTime()));
                statement.setString(3, record.getFieldName());
                statement.setString(4, record.getDatatype());
                for (int i = 0; i < record.getData().size(); ++i) {
                    statement.setDouble(i + 5, record.getData().get(i));
                }
                if (threeD) {
                    statement.setNull(8, Types.NULL);
                    statement.setNull(9, Types.NULL);
                    statement.setNull(10, Types.NULL);
                }


                statement.execute();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
